const path = require('path');

module.exports = {
  mode: 'production',//不会发出警告
  entry: './src/index.tsx',
  module: {
    rules: [{
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: /node_modules/ //如果是node_modules文件夹下的tsx 则不处理
    }]
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
}