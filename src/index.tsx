import * as _ from 'lodash';
//需要安装npm install @types/lodash

class Greeter {
  greeting: string;
  constructor(message: string) {
    this.greeting = message;
  }
  greet() {
    // return _.join(null,'');
    return _.join(["Hello,",' ', this.greeting], '')
  }
}

let greeter = new Greeter('world');

// let button = document.createElement('button');
// button.textContent = "Say Hello";
// button.onclick = function () {
  alert(greeter.greet());
// }

// document.body.appendChild(button);